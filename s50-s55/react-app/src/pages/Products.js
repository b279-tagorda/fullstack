
import ProductCard from '../components/ProductCard'
import { useEffect, useState} from "react"


export default function Products(){
	// Checks to see if mock data was captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// State that will be used to store courses retrieved from db

	const [AllProducts, setAllProducts] = useState([]);

	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllCourses(data.map(product => {
				return (
					<CourseCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])


	return(
		<>
			<h1>Products</h1>
			{/*Prop making ang prop passing*/}
			{AllProducts}
		</>
		)
}