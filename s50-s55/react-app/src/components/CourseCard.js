import { Card, Button } from 'react-bootstrap';
// In React.js we have 3 hooks
/*
1. useState
2. useEffect
3. useContext
*/

import { useState } from "react"
import {Link} from "react-router-dom"

export default function ProductCard({productProp}) {
    // Checks if props was successfully passed
    console.log(productProp.name);
    // checks the type of the passed data
    console.log(typeof productProp);

   // Desturing the courseProp into their own variables
    const { _id, name, description, price } = productProp;
/* 
    // useState
    // Used for storing different states
    // used to keep track of information to individual components

    // SYNTAX -> const [getter, setter] = useState(initialGetterValue);

    const [count, setCount] = useState(0);
    console.log(useState(0));

    const [seat, setSeat] = useState(30);

    // Function that keeps tract of the enrollees for a course
    function enroll(){
        if(seat > 0){
            setCount(count + 1);
            setSeat(seat - 1);
        }else{
            alert("No more seats available!");
        }
        
        console.log("Enrollees: " + count);
        console.log("Available Seats: " + seat);
    }*/

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
