// get POST data

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json()) //<<<<<-----converts to JSON
.then(data => showPost(data));


// Add post data
// Document object manipulation


document.getElementById("form-add-post")
.addEventListener("submit", event => {

	// prevents our webpage to having unwanted "refresh" after listening
	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.getElementById("txt-title").value,
			body: document.getElementById("txt-body").value,
			userId: 1
		}),
		headers: {"Content-Type" : "application/json; charset=UTF-8"}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert("Successfully Added");

		document.getElementById("txt-title").value = null;
		document.getElementById("txt-body").value = null;
	})

})


// Show all posts

const showPost = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>

			</div>

		`;
	})


	document.getElementById("div-post-entries").innerHTML = postEntries;
}


// Edit Post

const editPost =  (id) => {
	let title = document.getElementById(`post-title-${id}`).innerHTML;
	let body = document.getElementById(`post-body-${id}`).innerHTML;


	// Passing of data in the Edit post secttion

	document.getElementById("txt-edit-id").value = id;
	document.getElementById("txt-edit-title").value = title;
	document.getElementById("txt-edit-body").value = body;
	document.getElementById("btn-submit-update").removeAttribute("disabled");
}

// Updating a post
document.getElementById("form-edit-post")
.addEventListener("submit", event => {
	event.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method : "PUT",
		body: JSON.stringify({
			id: document.getElementById("txt-edit-id").value,
			title: document.getElementById("txt-edit-title").value,
			body:  document.getElementById("txt-edit-body").value,
			userId: 1
		}),
		headers: { "Content-Type" : "application/json; charset=UTF-8"}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		alert("Successully updated!");



		document.getElementById("txt-edit-id").value = null;
		document.getElementById("txt-edit-title").value = null;
		document.getElementById("txt-edit-body").value = null;
		document.getElementById("btn-submit-update").setAttribute("disabled", true);
	})
})


// DELETING A POST



const deletePost =  (id) => {

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		method : "DELETE",
		headers: { "Content-Type" : "application/json"},
	})
	.then(res => {
        const postElement = document.getElementById(`post-${id}`)
        postElement.remove();
		alert("Successully deleted!");
	})
}

// ALTERNATE from SIR ROME
// // Deleting a post
// const deletePost = (id) => {
//     fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: 'DELETE' });
//     document.getElementById(`post-${id}`).remove();
// }