import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Row, Col} from "react-bootstrap"
import { useParams } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function ProductView(){

	const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])


	const addToCart = (productId) => {
		if(localStorage.getItem("isAdmin") === "true"){
			Swal.fire({
				title: "OOPS!",
				icon: 'error',
				text: "An admin cannot do this action."
			});

			console.log(localStorage.getItem("isAdmin"));
		}else{
			fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
				method: "POST",
				headers: {
					"Content-type" : "application/json",
					Authorization : `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);


				if(data === true){
					Swal.fire({
					  title: 'Sweet!',
					  text: 'Order has been received!',
					  imageUrl: '../images/GlazedLogo.png',
					  imageWidth: 300,
					});

				}else{
					Swal.fire({
						title: "Something Went Wrong!",
						icon: 'error',
						text: "There is an error, please try again!"
					});
				}
			})
		}
	}


	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<Button variant="primary" onClick={() => addToCart(productId)}>Add to Cart</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		);
}