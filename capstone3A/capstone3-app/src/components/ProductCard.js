import { Card } from 'react-bootstrap';
// In React.js we have 3 hooks
/*
1. useState
2. useEffect
3. useContext
*/

import { useState } from "react"
import {Link} from "react-router-dom"

export default function ProductCard({productProp}) {

    // console.log(productProp.name);
    // console.log(typeof productProp);

   // Desturing the courseProp into their own variables
    const { _id, name, description, price } = productProp;

    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
