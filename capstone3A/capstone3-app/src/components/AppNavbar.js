import {Nav, Navbar, NavDropdown, Container} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { useState, useContext, useEffect } from "react"
import UserContext from "../UserContext"

export default function AppNavbar(){

  const { user } = useContext(UserContext);
  const userFullName = `${user.firstName} ${user.lastName}`;
    // console.log(user);


	return(
	<Navbar expand="lg"  className="sticky-top bg-success m-0 px-2">
      <Container fluid>
          <Navbar.Brand as={ NavLink } to="/" end className="font-weight-bold">
            <img
                alt=""
                src={require('../images/SmallLogo.png')}
                height="30"
                className="d-inline-block align-top rounded"
                />{' '}
              Glazed
            </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">

      {user.isAdmin 
        ? 

          (
              <Nav.Link as={NavLink} to="/dashboard" end>
                Dashboard
              </Nav.Link>
          ) 
        : 

          (
              <>
                <Nav.Link as={NavLink} to={"/"}>Home</Nav.Link>
                <Nav.Link as={NavLink} to={"/products"}>Menu</Nav.Link>
                <Nav.Link as={NavLink} to={"/branches"}>Branches</Nav.Link>
              </>
            )}

      {user.id !== null 
        ? 

          (
              <NavDropdown title={userFullName} align="end" id="collasible-nav-dropdown" end>
              
                <NavDropdown.Item as={NavLink} to="/settings" end>
                  Account Settings
                </NavDropdown.Item>

                <NavDropdown.Item as={NavLink} to="/report" end>
                  Report
                </NavDropdown.Item>

                <NavDropdown.Divider />
                <NavDropdown.Item as={NavLink} to="/logout" end>
                  Logout
                </NavDropdown.Item>

              </NavDropdown>
          ) 
        : 

          (
              <>
                <Nav.Link as={NavLink} to={"/register"}>Register</Nav.Link>
                <Nav.Link as={NavLink} to={"/login"}>Login</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
  </Navbar>
  );
}