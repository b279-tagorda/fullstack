// import courseData from "../data/courses"
import ProductCard from '../components/ProductCard'
import { useEffect, useState} from "react"


export default function Products(){


	const [AllProducts, setAllProducts] = useState([]);

	// Retrieves the courses from database upon initial render of the "Courses" Component

	useEffect(() => {
		fetch(`http://localhost:4000/products/all`)
		.then(res => res.json())
		.then(data => {
			// console.log(data);


			setAllProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])


	return(
		<>
			<h1>Our Menu</h1>
			{/*Prop making ang prop passing*/}
			{AllProducts}
		</>
		)
}