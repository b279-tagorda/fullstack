import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import Banner from "../components/Banner";
import HotDeals from "../components/HotDeals";
import HomeCarousel from "../components/HomeCarousel";
import logo from "../images/GlazedLogo.png"
import FeaturedProducts from "../components/FeaturedProducts";

export default function Error(){


	
	const { user } = useContext(UserContext);
	const data = {
		title: "404: ERROR!",
		content: "The product or page you are looking cannot be found.",
		destination: "/",
		label: "Home",
		image: {logo}
	}

	return(
		<>
		{
        (user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<div className="p-5">
			<Banner bannerProp={data}/>
			<FeaturedProducts/>
		</div>
		}
		</>
		
	)
}