import Banner from "../components/Banner"
import React, { useState } from 'react';

export default function Home(){

	const data = {
		title: 	<img
					alt=""
	            	src={require('../images/GlazedLogo.png')}
	            	height="400"
	            	className="d-inline-block align-center rounded"
	           		 />,
		content: "First Unlimited Wings with Cheese Fondue!",
		destination: "/products",
		label: "Our Branches"

	}


	return(

		<>
				<Banner bannerProp={data} />

		</>


		);
}



