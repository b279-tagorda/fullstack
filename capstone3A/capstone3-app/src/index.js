import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import { createRoot } from 'react-dom/client';




// Importing react-bootstrap
// npm install react-bootstrap bootstrap (command)
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);




