const Order = require("../models/Order.js")
const Product = require("../models/Products.js")
const auth = require("../auth");

const User = require("../models/Users")


// Add order
module.exports.addToOrder = async (user, reqParams, reqBody) => {
  if (user.isAdmin) {
    return true;
  } else {
    let product = await Product.findById(reqParams.productId);

    let subTotal = reqBody.quantity * product.price;

    let newOrder = new Order({
      userId: user.id,
      product: [
        {
          productId: product.id,
          name: product.name,
          quantity: reqBody.quantity,
          price: product.price
        },
      ],
      subTotal: subTotal,
    });

    return newOrder.save().then(
    (order, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  }
};

