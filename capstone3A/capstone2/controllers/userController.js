const User = require('../models/Users');

const bcrypt = require('bcrypt');

const auth = require('../auth');

const Product = require('../models/Products');


//Check if the email already exist

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,

		password: bcrypt.hashSync(reqBody.password, 10)
			 // bcrypt.hashSync encrypts the password , 10 = minimum salt rounds for encryption
	})


	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});

};

module.exports.getProfile = (reqBody) => {

    return User.findById(reqBody).then(result => {
        if (result == null) {
            return false;
        }else {
            result.password = ""

            return result;
        }
    })

};



// function to login a user.
module.exports.loginUser = (reqBody) => {

	return  User.findOne({email: reqBody.email}).then(result => {
		console.log(result)
		if(result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,
				result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {

                return false;
		};
        };
	});

};






// 
///////////////////////////////////////////////////////////////////////

module.exports.getUserProfile = (reqParams, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){	

    return User.findById(reqParams.userId).then(result => {
        if (result == null) {
            return false;
        }else{
            result.password = ""

            return result;
        
	}
    })
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
}

/////=======================SG
//

//// SETTING IS ADMIN STATUS TRUE/FALSE
module.exports.setAsAdmin = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let changeUser = {
		isAdmin : reqBody.isAdmin
		}

		return User.findByIdAndUpdate(reqParams.userId, changeUser).then((user, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
};


module.exports.createOrder = async (data) => {
    if(data.isAdmin){
        return false
    } else {
        let isUserUpdated = await User.findOne({_id: data.userId}).then(userRes => {
            Product.findById({_id: data.product.productId}).then(prodRes => {
                 userRes.orderedProduct.push({
                    products: {
                        productId: data.product.productId,
                        productName: prodRes.name,
                        quantity: data.product.quantity
                    },
                    totalAmount: prodRes.price * data.product.quantity,
                })

                return userRes.save().then((res, err) => {
                    if(err) {
                        return false
                    } else {
                        return true
                    }
                })
            })
        })

        let isOrderedProduct =  await Product.findOne({_id: data.product.productId}).then(res => {
            res.userOrders.push({
                userId: data.userId
            })

            return res.save().then((res, err) => {
                if(err){
                    return false
                } else {
                    return true
                }
            })
        })

        if(isUserUpdated && isOrderedProduct){
            return Promise.resolve(true)
        } else {
            return Promise.resolve(false)
        }
    }
}


                        // async
// module.exports.checkout = async (data) => {
//     if(!data.isAdmin){
//       //       if (!Array.isArray(data.products)) {
//       // return Promise.resolve("Invalid products data");
//     // }
//         let productTemp = [];
//         let totalAmountTemp = 0;
//         let isProductUpdated;
//         let products = data.products

//         products.forEach(singleProduct => {
//             console.log(singleProduct);
//             Product.findById(singleProduct.productId).then(product => { 

//              console.log(product)

//                 productTemp.push(
//                     {
//                         productId : singleProduct.productId,
//                         productName : product.name,
//                         quantity : singleProduct.quantity
//                     }
//                 )
//                 totalAmountTemp = totalAmountTemp + (product.price * singleProduct.quantity)
//             }
//             )
//             isProductUpdated = Product.findById(singleProduct.productId).then(product =>{

//                 product.userOrders.push({userId : data.userId})
//                 return product.save().then((product, error) => {
//                     if(error){
//                         return false;
//                     } else {
//                         return true;
//                     }
//                 })
//             })
//         });


//         let isUserUpdated = await User.findById(data.userId).then(user => {
//                 user.orderedProduct.push(
//                     {

//                         product : productTemp,
//                         totalAmount : totalAmountTemp
//                     }
//                 );
//                 return user.save().then((user, error) => {
//                     if(error){
//                         return false;
//                     }else{
//                         return true ;
//                     }
//                 })
//         })


//         if(isUserUpdated && isProductUpdated){
//             return true;
//         }else{
//             return false;
//         }
//     } else {
//         return Promise.resolve("Admins can't checkout products")
//     }
// };


