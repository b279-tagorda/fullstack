const Product = require("../models/Products");


// Create a new product
module.exports.addProduct = (data) => {
	console.log(data.isAdmin);
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			imgSource: data.product.imgSource,
			description: data.product.description,
			price: data.product.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return error;
			}
			return product;
		})
	} 

	// If the user is not admin, then return this message as a promise to avoid errors
	let message = Promise.resolve("Please log in using an Admin account.");

	return message.then((value) => {
		return value
	})
}


// retrive all products function
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}


// retrive all active function
module.exports.getAllActive = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}


// retrive specific post
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((course, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
}




// archiving a product
module.exports.archieveProduct = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);

	if(isAdmin){
		let changeProduct = {
		isActive : reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, changeProduct).then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		let message = Promise.resolve("You don't have the access rights to do this action.");

    return message.then((value) => {
        return value
    })
	}
};

