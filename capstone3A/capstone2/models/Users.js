const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	
	orderedProduct: [{
		product: [{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String
				// required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Number is required"]
			}
		}],
		totalAmount: {
				type: Number
				// required: [true, "Amount is required"]
			},
			purchaseOn: {
				type: Date,
				default: new Date()
			}
	}]
	
});

        //     product :
        //     	[
        //     	{
        //         productId : {
        //             type : String,
        //             required : true
        //         },    
        //         productName : {
        //             type : String,
        //             required : true
        //         },
        //         quantity: {
        //             type : Number,
        //             required : true
        //         }
        //     }
        //     ],
        //     totalAmount : {
        //         type : Number,
        //         required : true
        //     },
        //     purchasedOn:{
        //         type: Date,
        //         default: new Date("2023-01-01T12:59:22")
        //     }

        // }


module.exports = mongoose.model("User", userSchema);
