const mongoose = require("mongoose");

const productsSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "PRODUCT NAME is required!"]
	},
	description : {
		type : String,
		required : [true, "PRODUCT DESCRIPTION is required!"]
	},
	price : {
		type : Number,
		required : [true, "PRODUCT PRICE is required!"]
	},
	imgSource : {
		type : String,
		required : [true, "PRODUCT IMAGE is required!"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression instantiates the current date
		default : new Date()
	},
	userOrders : [{
		userId : {
			type : String,
			required : [true, "user ID is required"] 
		}
	}]


})


module.exports = mongoose.model("Products", productsSchema);