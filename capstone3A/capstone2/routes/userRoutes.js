const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController.js')
const auth = require("../auth.js")


// route for checking if email already exist.
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});



// route for registering a user.`1`
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});



// route for user login
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// route for get profile
// router.post("/details", auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)

// 	userController.getProfile(userData.id).then(resultFromController => res.send(resultFromController))
// });


router.post("/:userId/userDetails", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getUserProfile(req.params, isAdmin).then(resultFromController => res.send(resultFromController));

})


// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in 
router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization).id;

	// Provides the user's ID for the getProfile controller method
	userController.getProfile(userData).then(resultFromController => res.send(resultFromController));

});




// create at order

router.post("/checkOut", auth.verify, (req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		products : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
})



///////////////////////////////============SG

router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setAsAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
									//////req.params nasa URL

})


// create orders


router.put("/orderProduct", (req, res) => {
    let data = {
        product: req.body,
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    userController.createOrder(data).then(resultFromController => res.send(resultFromController))
})






// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;