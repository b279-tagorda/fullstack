const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController")
const auth = require("../auth");


// Add order
router.post("/:productId/addToOrder", auth.verify, (req, res) => {
  const user = auth.decode(req.headers.authorization);

  orderController.addToOrder(user, req.params, req.body).then((resultFromController) => res.send(resultFromController));
});



module.exports = router;
