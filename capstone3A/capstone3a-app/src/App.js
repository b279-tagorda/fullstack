import './App.css';

import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react";
import { UserProvider } from "./UserContext";

import AppNavbar from "./components/AppNavbar";
// import AppSideNav from "./components/AppSideNav";
// import Footer from "./components/Footer";

import Register from "./pages/Register";
import Home from "./pages/Home"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Dashboard from './pages/Dashboard';
import AllProducts from './pages/AllProducts';
import Products from './pages/Products';
import CheckOut from './pages/CheckOut';
import BranchPage from './pages/BranchPage';
import Error from './pages/Error';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear(); 
  }

  useEffect(()=>{
    console.log(user);
    console.log(user);
    console.log(localStorage);
  }, [user])
  useEffect(()=>{

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data._id !== undefined){
        console.log(" makuha");
        setUser({
            id: data._id,
            firstName : data.firstName,
            lastName : data.lastName,
            email : data.email,
            mobileNo : data.mobileNumber,
            isAdmin: data.isAdmin
        });
      }
      else{
        console.log("di makuha");
        setUser({
          
          id: null,
          isAdmin: null
        });
      }
      
    })

  }, [])
  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid className='p-0 m-0'>
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/BranchPage" element={<BranchPage />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="/dashboard" element={<Dashboard />} />
            <Route exact path="/products/allProducts" element={<AllProducts/>} />
            <Route exact path="/products" element={<Products/>} />
            <Route exact path="/products/buy/:productId" element={<CheckOut />} />
            <Route exact path="*" element={<Error />} />
          </Routes>
        </Container>
        </Router>
    </UserProvider>
    </>
    

  );
}

export default App;






























// import './App.css';
// import AppNavbar from "./components/AppNavbar";
// import Home from "./pages/Home";
// import Products from "./pages/Products";
// import Register from "./pages/Register";
// import Login from './pages/Login';
// import Logout from './pages/Logout';
// import Error from "./pages/Error";
// import ProductView from "./components/ProductView";
// import Dashboard from './pages/Dashboard';
// import AllProducts from './pages/AllProducts';
// import CheckOut from './pages/CheckOut';
// import Branches from './pages/Branches';

// // npm install react-router-dom
// import { Route, Routes } from "react-router-dom"
// import { BrowserRouter as Router } from "react-router-dom"
// import { Container } from "react-bootstrap";
// import { useState, useEffect } from "react"
// import { UserProvider } from "./UserContext"

// // React JS is a single page application (SPA)
// // Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// // When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// // It renders the component executing the function component and it's expressions
// // After rendering it mounts the component displaying the elements
// // Whenever a state is updated or changes are made with React JS, it rerenders the component
// // Lastly, when a different page is loaded, it unmounts the component and repeats this process
// // The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

// function App() {

//   // Creating a user state for global scope

//   /*const [user, setUser] = useState({
//     email: localStorage.getItem("email")
//   })*/

//   const [user, setUser] = useState({
//       id: localStorage.getItem("id"),
//       isAdmin: localStorage.getItem("isAdmin"),
//       email: localStorage.getItem("email"),
//       token: localStorage.getItem("token")
//   })

//   // Function for clearing the storage on logout
//   const unsetUser = () => {
//     localStorage.clear();
//   }

//   useEffect(() => {
//     // console.log(user);
//     // console.log(localStorage);
//   }, [user])



//   return (
//     <UserProvider value={{user, setUser, unsetUser}}>
//       <Router>
//         <AppNavbar/>
//           <Container id="HomeBackground">
//               <Routes>
//                   <Route path="/" element={<Home/>}/>
//                   <Route path="/products" element={<Products/>}/>
//                   <Route path="/ProductView/:productId" element={<ProductView/>}/>
//                   <Route path="/register" element={<Register/>}/>
//                   <Route path="/login" element={<Login/>}/>
//                   <Route path="/logout" element={<Logout/>}/>
//                   <Route path="/branches" element={<Branches/>}/>
//                   <Route path="/dashboard" element={<Dashboard />} />
//                   <Route path="/products/allProducts" element={<AllProducts/>} />
//                   <Route path="/products" element={<Products/>} />
//                   <Route path="/products/buy/:productId" element={<CheckOut />} />
//                   <Route path="*" element={<Error/>}/>
//               </Routes>
//           </Container>
//       </Router>
//     </UserProvider>
//   );
// }

// export default App;
