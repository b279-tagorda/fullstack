// import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedProducts() {
    return (
        <div className='d-flex flex-column my-5  p-5 text-success'>
        <h2 className="text-center">Dine-in Exclusive Promos!</h2>
        <Row className="my-3 text-dark h-100">
            <Col className="my-2" xs={12} md={6} lg={4}>
                <Card className="card-height square border border-2 border-success rounded" >
                <Card.Img  variant="top"  src={require('../images/Promo/IcedTea.jpg')} />
                <Card.Header className="bg-success">
                <Card.Title className="text-light text-center">Unlimited Iced Tea</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Card.Text>
                    BEAT THE HEAT!
                    
                    Enjoy Unlimited Iced Tea by just adding P20 pesos on your unlimited wings! 🙂

                    Promo Runs from April 3 to April 9 ONLY.
                    #glazedunlimitedwings
                    #unlimitedicedtea
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>

            <Col className="my-2" xs={12} md={6} lg={4}>
                <Card className="card-height square border border-2 border-success rounded">
                <Card.Img variant="top"  src={require('../images/Promo/hbd.jpg')}  />
                <Card.Header className="bg-success">
                    <Card.Title className="text-light text-center">Birthday Promo</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text>
                    Celebrate your Birthday with us at Glazed Unlimited Wings Fairview - Lagro Branch and get a FREE CHEESE FONDUE on us on your entire birth month!
                    Grab 4 of your friends (plus you) to avail this promo! Just present us your ID showing your birthday 🥰
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>


            <Col className="my-2" xs={12} md={6} lg={4}>
                <Card className="card-height square border border-2 border-success rounded">
                <Card.Img variant="top"  src={require('../images/Promo/loyalty.jpg')}  />
                <Card.Header className="bg-success">
                    <Card.Title className="text-light text-center">Loyalty Card</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text>
                   Get your Loyalty card now on Glazed Lagro branch now and start earning your stickers to get a FREE Cheese Fondue on your 6th visit and FREE Unlimited Wings after your 12th visit!

                    We are open Monday to Sunday from 11am to 9pm!
                    Located 2f above BPI Lagro branch 🙂
                    </Card.Text>
                </Card.Body>
                </Card>
            </Col>


        </Row>
        </div>
    );
}
