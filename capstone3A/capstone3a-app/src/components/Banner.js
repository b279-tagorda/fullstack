// // object destructuring
// import { Row, Col, Button } from "react-bootstrap";
// import { Link } from "react-router-dom";

// export default function Banner({bannerProp}){
// 	// console.log(bannerProps);

// 	const {title, content, destination, label, image} = bannerProp;

// 	return(
// 			<Row>
// 				<Col className="p-5 text-center">
// 					<h1>{title}</h1>
// 					<p>{content}</p>
// 					<Button as={Link} to={destination} className="btn-primary">{label}</Button>
// 				</Col>
// 			</Row>
// 		);
// }


import { Link } from "react-router-dom";
import { Row, Col, Button, Image } from "react-bootstrap";
// import logo from "../images/GlazedLogo.png"

export default function Banner({bannerProp}){
	const {title, content, destination, label} = bannerProp;
	return(
        <div className="vh-10 text-light d-flex align-items-center justify-content-center">
		<Row className="align-items-center justify-content-center">
			<Col className="text-center d-flex flex-column justify-content-center align-items-center m-4" lg={12}>
                <Image
                className="d-block w-50  carousel-height"
                src={require('../images/GlazedLogo.png')}
                fluid
                />
				<h1 className="text-success">{title}</h1>
            	<h4 className="text-success">{content}</h4>
				<Button className="w-50 btn-outline-success mt-2" as = {Link} to={destination} variant="light">{label}</Button>
			</Col>
		</Row>
        </div>
	)
}