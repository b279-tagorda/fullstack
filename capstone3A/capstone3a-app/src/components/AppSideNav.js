import { useContext } from "react"
import UserContext from "../UserContext";
import {Navigation} from 'react-minimal-side-navigation';
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
import AllInboxIcon from '@mui/icons-material/AllInbox';
import BentoIcon from '@mui/icons-material/Bento';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import RecentActorsIcon from '@mui/icons-material/RecentActors';
// import AddIcon from '@mui/icons-material/Add';
// import ArchiveIcon from '@mui/icons-material/Archive';
// import SyncAltIcon from '@mui/icons-material/SyncAlt';
import { NavLink } from "react-router-dom";

import { Nav } from "react-bootstrap";



export default function AppSideNav(){

    const { user } = useContext(UserContext);
    console.log(user);
    return(
        <div className="fixed">
        <Navigation className="SideBar"
            // you can use your own router's api to get pathname
            activeItemId=""
            onSelect={({itemId}) => {
            // maybe push to the route
            }}
            items={[
            {
                title: 'User Management',
                itemId: '/user',
                elemBefore: () => <ManageAccountsIcon />,
                subNav: [
                {
                    title: 'All Users',
                    elemBefore: () => <AssignmentIndIcon />,
                    itemId: '/users',
                },
                {
                    title: 'Archived Users',
                    elemBefore: () => <RecentActorsIcon />,
                    itemId: '/users/archived',
                },
                ],
            },

            {
                title: 'Product Management',
                itemId: '/products',
                elemBefore: () => <AllInboxIcon />,
                subNav: [

                // {
                //     title:<Nav.Link as={ NavLink } to="/products/addNewProduct" end>Add Products</Nav.Link>,
                //     elemBefore: () => <AddIcon />,
                //     itemId: '/products/addNewProduct',
                // },

                {
                    title: <Nav.Link as={ NavLink } to="/products/allProducts" end>All Products</Nav.Link>,
                    elemBefore: () => <BentoIcon />,
                    itemId: '/products/all',
                },

                // {
                //     title:<Nav.Link as={ NavLink } to="/products/archivedProduct" end>Archived Products</Nav.Link>,
                //     elemBefore: () => <ArchiveIcon />,
                //     itemId: '/products/archivedProduct',
                // },

                // {
                //     title:<Nav.Link as={ NavLink } to="/products/updateProduct" end>Update Products</Nav.Link>,
                //     elemBefore: () => <SyncAltIcon />,
                //     itemId: '/products/updateProduct',
                // },


                ],
            },
            ]}
        />
        </div>
    )
}