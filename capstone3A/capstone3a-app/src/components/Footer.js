import React from "react";
import { Row, Container } from "react-bootstrap";

const Footer = () => (
  <div className="footer bg-success bg-success m-0 px-2 text-light" expand="lg">
  	<Container className="fluid">
      <Row>
  		    <h6 className="pt-2">Visayas Ave | Lagro | Gastambide | Marikina</h6>
          <h7 className="">Copyright ⓒ 2023 Glazed Unlimited Wings. All Rights Reserved.</h7>
      </Row>
    </Container>
  </div>
);

export default Footer;