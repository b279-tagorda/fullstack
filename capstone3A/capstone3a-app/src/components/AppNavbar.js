import {Nav, Navbar, NavDropdown, Container} from "react-bootstrap"
import { NavLink } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext"

//remove link from navlink and useState,useEffect from useContext
export default function AppNavbar(){

  const { user } = useContext(UserContext);
  const userFirstName = `${localStorage.getItem("firstName")}`;
    console.log(user);


	return(
	<Navbar expand="lg"  className="sticky-top bg-success m-0 px-2 text-light">
      <Container fluid>
          <Navbar.Brand as={ NavLink } to="/" end className="font-weight-bold">
            <img
                alt=""
                src={require('../images/GlazedLogo2.png')}
                height="40"
                className="d-inline-block align-top rounded mx-2"
                />
            </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">

      {localStorage.getItem("isAdmin")  === "true"
        ? 

          (
              <Nav.Link as={NavLink} to="/dashboard" end className="text-light">
                Dashboard
              </Nav.Link>
          ) 
        : 

          (
              <>
                <Nav.Link as={NavLink} to={"/"} className="text-light">Home</Nav.Link>
                <Nav.Link as={NavLink} to={"/products"} className="text-light">Menu</Nav.Link>
                <Nav.Link as={NavLink} to={"/BranchPage"} className="text-light">Branches</Nav.Link>
              </>
            )}

      {localStorage.getItem("isAdmin") !== null 
        ? 

          (
              <NavDropdown className="Navdown text-success" title={`Welcome! ${userFirstName}`} align="end" end >
              
  {/*              <NavDropdown.Item as={NavLink} to="/settings" className="text-success" end>
                  Account Settings
                </NavDropdown.Item>*/}

{/*            {
              localStorage.getItem("isAdmin") === "true"
        ?
               <NavDropdown.Item as={NavLink} to="/report" className="text-success" end>
                  Report
              </NavDropdown.Item>

        :
                <NavDropdown.Item as={NavLink} to="/orders" className="text-success" end>
                  Orders
              </NavDropdown.Item>

            }*/}
 {/*               <NavDropdown.Divider />*/}
                <NavDropdown.Item as={NavLink} to="/logout" className="text-success" end>
                  Logout
                </NavDropdown.Item>

              </NavDropdown>
          ) 
        : 

          (
              <>
                <Nav.Link as={NavLink} to={"/register"} className="text-light">Register</Nav.Link>
                <Nav.Link as={NavLink} to={"/login"} className="text-light">Login</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
  </Navbar>
  );
}