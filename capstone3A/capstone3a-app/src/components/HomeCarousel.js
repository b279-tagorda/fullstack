import {Carousel, Image} from 'react-bootstrap';

export default function HomeCarousel() {
return (
    <div className="pt-1" xs={12}>
    <Carousel className='vh-75 w-100 d-inline-block mb-5' fade indicators={false}>


    <Carousel.Item xs={12}>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/carousel/c6.jpg')}
        alt="Third slide"
        />

        <Carousel.Caption className=' fluid d-flex flex-column align-items-center justify-content-center vh-25' xs={12}>
        <div  className='carousel-text'>
        <h1>Solo? on a date? or with your family and friends?</h1>
        <h3>We have ala carte and set menu to mix and match</h3>
        </div>
        </Carousel.Caption>
    </Carousel.Item>

    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit  carousel-height carousel-caption-bg"
        src={require('../images/carousel/c1.jpg')}
        alt="First slide"
        />
        <Carousel.Caption className=' fluid d-flex flex-column align-items-center justify-content-center vh-25' xs={12}>
        <div  className='carousel-text'>
        <h1>Craving for Chicken Wings for your next getaway?</h1>
        <h3>We got you! Our Bilao is available in 30pcs and 50 pcs</h3>
        </div>
        </Carousel.Caption>
    </Carousel.Item>


    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/carousel/c4.jpg')}
        alt="Third slide"
        />

        <Carousel.Caption className='fluid d-flex flex-column align-items-center justify-content-center vh-25' xs={12}>
        <div  className='carousel-text'>
        <h1>Level up your Chicken Wings Experience!</h1>
        <h3>Try our Cheese Fondue. Made with mozarella, cheddar and Glazed Cheese sauce</h3>
        </div>
        </Carousel.Caption>
    </Carousel.Item>


    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/carousel/c7.jpg')}
        alt="Third slide"
        />

        <Carousel.Caption className='fluid d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Spicy Lover and want to try something new?</h1>
        <h3>Try our signature flavor, Spicy Coffee.</h3>
        </div>
        </Carousel.Caption>
    </Carousel.Item>

    <Carousel.Item>
        <Image
        className="d-block mx-auto img-fit carousel-height carousel-caption-bg"
        src={require('../images/carousel/c2.jpg')}
        alt="Second slide"
        />

        <Carousel.Caption className='fluid d-flex flex-column align-items-center justify-content-center vh-25'>
        <div  className='carousel-text'>
        <h1>Eating Chicken Wings always makes me feel better</h1>
        <h3>Click our menu now! or visit our nearest branch!</h3>
        </div>
        </Carousel.Caption>
    </Carousel.Item>


    </Carousel>
    </div>
);
}
