// import { Card } from 'react-bootstrap';
// // In React.js we have 3 hooks
// /*
// 1. useState
// 2. useEffect
// 3. useContext
// */

// import { useState } from "react"
// import {Link} from "react-router-dom"

// export default function ProductCard({productProp}) {

//     // console.log(productProp.name);
//     // console.log(typeof productProp);

//    // Desturing the courseProp into their own variables
//     const { _id, name, description, price } = productProp;

//     return (
//         <Card className="my-3">
//             <Card.Body>
//                 <Card.Title>{name}</Card.Title>
//                 <Card.Subtitle>Description:</Card.Subtitle>
//                 <Card.Text>{description}</Card.Text>
//                 <Card.Subtitle>Price:</Card.Subtitle>
//                 <Card.Text>{price}</Card.Text>
//                 <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
//             </Card.Body>
//         </Card>
//     )
// }

import { Card, Button, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";

import { BsChevronUp, BsChevronDown } from "react-icons/bs";

import Banner from './Banner';


export default function ProductCard({productProp}) {


    const { _id, name, description, quantity, price, imgSource } = productProp;



    return (
        <>
        <Col className="pb-4" xs={12} md={4} lg={2}>
            <Card className=" card-height  card-border shadow-md card-bg">
            <Card.Img className='img-fluid w-100 product-img-fit'
                src={imgSource}
            />
                <Card.Header className='d-flex align-items-center justify-content-center'>
                <Card.Title>
                    {name}
                </Card.Title>
                </Card.Header>
                <Card.Body>

                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price:
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>
                <Card.Text>
                    Quantity:

                      {quantity}

                </Card.Text>

            </Card.Body>
            <Card.Footer>
            <Button className='w-100 btn-outline-light' variant='success' as={Link} to={`/products/buy/${_id}`}>BUY</Button>
            </Card.Footer>
        </Card>
            </Col>
        
        </>
    )
}
