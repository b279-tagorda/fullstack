import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

import video1 from "../videos/ran.mp4"
import video2 from "../videos/joseph.mp4"
import video3 from "../videos/brent.mp4"

import React from 'react';
import ReactPlayer from 'react-player';



export default function Vloggers() {
    return (
        <div className='d-flex fluid flex-column m-2 p-3 text-primary mx-auto align-items-center justify-content-center mb-5 bg-success'>
        <h2 className="text-center text-light pt-2">Featured Videos</h2>
        <Row className="my-3 h-100">
            <Col className="p-2 my-2" xs={12} md={6} lg={4}>
              	<video className="square border border-2 border-light rounded p-2"
              		src={video1}
       				controls={true}
       				width="360px"
        			height="640px"
     			/>
            </Col>

  
     

             <Col className="p-2 my-2"  xs={12} md={6} lg={4}>
              	<video className="square border border-2 border-light rounded p-2"
              		src={video2}
       				controls={true}
       				width="360px"
        			height="640px"
     			/>
            </Col>


            <Col className="p-2 my-2"  xs={12} md={6} lg={4}>
              	<video className="square border border-2 border-light rounded p-2"
              		src={video3}
       				controls={true}
       				width="360px"
        			height="640px"
     			/>
            </Col>


        </Row>
        </div>
    );
}
