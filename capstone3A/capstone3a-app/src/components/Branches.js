import { Row, Col, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Branches(){

	return(
		<Row className="m-3 pt-2 pb-3 bg-success mx-auto d-flex flex-row">
              <h2 className="text-center text-light">Our Branches</h2>
            <Col xs={12} md={3}>
                <Card className="cardHighlight mt-2 border border-success border-2 rounded">
                    <Card.Body>
                    <Card.Header className="bg-success d-flex align-items-center justify-content-center rounded">
                        <Button variant="success" as={Link} to={`https://www.facebook.com/glazedunliwings`} target="_blank">
                            <h3 className="text-center text-white">Visayas Ave</h3>
                        </Button>
                    </Card.Header>
                        <Card.Text>
                            <h5>Address:</h5>
                            <h6>📍 106 Visayas Avenue, Quezon City, Philippines</h6>

                             <h5>Store Hours:</h5>
                            <h6>⏰ 11:00am - 9:00pm</h6>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight mt-2 border border-success border-2 rounded">
                    <Card.Body>
                    <Card.Header className="bg-success d-flex align-items-center justify-content-center rounded">
                        <Button variant="success" as={Link} to={`https://www.facebook.com/GlazedUnlimitedWingsLagro`} target="_blank">
                            <h3 className="text-center text-white">Lagro</h3>
                        </Button>
                        </Card.Header>
                        <Card.Text>
                            <h5>Address:</h5>
                            <h6>📍 2nd Floor, B1 L7, Quirino Highway, Sacred Heart Village, Quezon City, Philippines</h6>

                            <h5>Store Hours:</h5>
                            <h6>⏰ 12:00pm - 10:00pm</h6>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={3}>
                <Card className="cardHighlight mt-2 border border-success border-2 rounded">
                    <Card.Body>
                        <Card.Header className="bg-success d-flex align-items-center justify-content-center rounded">
                        <Button variant="success" as={Link} to={`https://www.facebook.com/glazed.gastambide`} target="_blank">
                            <h3 className="text-center text-white">Gastambide</h3>
                        </Button>
                        </Card.Header>
                        <Card.Text>
                            <h5>Address:</h5>
                            <h6>📍 Amio Property, 639 Gastambide St., Sampaloc, Manila, Philippines</h6>

                            <h5>Store Hours:</h5>
                            <h6>⏰ 11:00am - 9:30pm</h6>
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
             <Col  xs={12} md={3}>
                <Card className="cardHighlight mt-2 border border-success border-2 rounded">
                    <Card.Body>
                        <Card.Header className="bg-success d-flex align-items-center justify-content-center rounded">
                        <Button variant="success" as={Link} to={`https://www.facebook.com/profile.php?id=100091239901727`} target="_blank">
                            <h3 className="text-center text-white">Marikina</h3>
                        </Button>
                        </Card.Header>
                        <Card.Text>
                            <h5>Address:</h5>
                            <h6>📍 VALP Building, L. De Guzman St. corner J. Molina St., Concepcion Uno, Marikina City, Philippines</h6> 

                            <h5>Address:</h5>
                            <h6>⏰ 11:00am - 9:00pm</h6>   
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
	)
}