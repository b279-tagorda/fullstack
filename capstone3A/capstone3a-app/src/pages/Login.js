import { useState, useEffect, useContext } from 'react';
import { Link, Navigate } from "react-router-dom";
import { Form, Button, Col, Row, Container } from 'react-bootstrap';
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import 'animate.css';

export default function Login() {

const { user, setUser } = useContext(UserContext);
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');

const [isActive, setIsActive] = useState(false);


useEffect(() => {

    if(email !== '' && password !== ''){
        setIsActive(true);
    }else{
        setIsActive(false);
    }

}, [email, password]);

function authenticate(e) {

    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"


        },
        body: JSON.stringify({
            email: email,
            password: password
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data.accessToken);
        if(data.access !== undefined){
            localStorage.setItem("token", data.access);
            retrieveUserDetails(data.access);

            // if(typeof data.access !== "undefined"){
            //     localStorage.setItem("token", data.access);
            //     retrieveUserDetails(data.access);



            // Swal.fire({
            //     title: "Login Successful",
            //     icon: "success",
            //     text: "Welcome to Glazed!"
            // })
            Swal.fire({
                title: 'Login Successful!',
                text: "Welcome to Glazed!",
                icon: "success",
                confirmButtonColor: '#379237',
                showClass: {
                popup: 'animate__animated animate__fadeIn'
                 },
                width: 400,
                padding: '1em',
                color: '#379237',
                hideClass: {
                popup: 'animate__animated animate__fadeOut'
                }
            });
        }
        else{
            Swal.fire({
                title: "Authentication Failed!",
                icon: "error",
                text: "Please check your login details and try again.",
                confirmButtonColor: '#379237',
                showClass: {
                popup: 'animate__animated animate__fadeIn'
                 },
                width: 400,
                padding: '1em',
                color: '#379237',
                hideClass: {
                popup: 'animate__animated animate__fadeOut'
                }

            });
        }
    });

    setEmail('');
    setPassword('');

}

const retrieveUserDetails = (token) => {


    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers:{
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        setUser({
                id: localStorage.setItem("id", data._id),
                firstName: localStorage.setItem("firstName", data.firstName),
                lastName: localStorage.setItem("lastName", data.lastName),
                email: localStorage.setItem("email", data.email),
                isAdmin: localStorage.setItem("isAdmin", data.isAdmin)

        });
    })
}

//from id
return(
    // (localStorage.getItem("id") !== null)
    (user.id !== null)
    ?
        <Navigate to="/dashboard" />
    :
    <>
        <Container fluid className="w-100 m-0 p-0">
        <Row className="w-100 m-0 p-0 fluid">
        <Col className="banner-bg vh-100 m-0 text-light d-flex flex-column align-items-center justify-content-center text-center p-5" xs={12} md={12} lg={7}>

                    <img fluid
                        alt=""
                        src={require('../images/homebg.png')}
                        height="750"
                        className="rounded"
                     />
        </Col>
            <Col xs={12} md={12} lg={5} className="d-flex flex-column align-items-center justify-content-center m-0 colr-bg ">
            <div className='w-75 vh-50  rounded bg-white my-5 d-flex flex-column align-items-center justify-content-center'>
            <h1 className="my-3 text-center">LOGIN</h1>
            
            <Form className="w-100 pb-5" onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail" className="mb-3">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {
            isActive
            ?
                <Button variant="primary" className='px-2 py-2 w-25' type="submit" id="submitBtn">
                    Login
                </Button>
            :
                <Button variant="danger" className='px-2 py-2 w-25' type="submit" id="submitBtn" disabled>
                    Login
                </Button>
            }
            <div className='d-flex flex-column align-items-center justify-content-center'>
            <p className='mt-5'>No account yet? <Link to={"/register"}><strong>Click Here!</strong></Link></p>
            </div>
        </Form>
        </div>
            </Col>
        </Row>
        </Container>
        </>
    )
}





// import { Form, Button, Row, Col, Container } from 'react-bootstrap';
// import { useState, useEffect, useContext } from 'react';
// import UserContext from "../UserContext"
// import { Navigate, Link } from "react-router-dom";
// import Swal from "sweetalert2"

// export default function Login() {
//     const { user, setUser } = useContext(UserContext);

//     // State hooks to store the values of the input fields
//     const [email, setEmail] = useState('');
//     const [password, setPassword] = useState('');
//     // State to determine whether submit button is enabled or not
//     const [isActive, setIsActive] = useState(false);

//     useEffect(() => {

//         // Validation to enable submit button when all fields are populated and both passwords match
//         if(email !== '' && password !== ''){
//             setIsActive(true);
//         }else{
//             setIsActive(false);
//         }

//     }, [email, password]);

//     function authenticate(e) {

//         // Prevents page redirection via form submission
//         e.preventDefault();

//         // Process a fetch request to the corresponding API

//         fetch(`http://localhost:4000/users/login`, {
//             method: "POST",
//             headers: {
//                 "Content-type" : "application/json"
//             },
//             body: JSON.stringify({
//                 email : email,
//                 password: password
//             })
//         })
//         .then(res => res.json())
//         .then(data => {
//             console.log(data.accessToken);

//             // If no user info is found, the "access" property will not be available
//             if(typeof data.access !== "undefined"){
//                 localStorage.setItem("token", data.access);
//                 retrieveUserDetails(data.accessToken);

//                 Swal.fire({
//                   icon: 'success',
//                   title: 'Login Successful!',
//                   text: 'Welcome!',
//                 })

//             }else{
//                 Swal.fire({
//                   icon: 'error',
//                   title: 'Authentication Failed!',
//                   text: 'Please try again!',
//                 })


//             }
//         })

//         setEmail('');
//         setPassword('');

//     }

//      // Retrieve user details using its token
//     const retrieveUserDetails = (token) => {
//         fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
//             headers: {
//                 Authorization: `Bearer ${token}`
//             }
//         })
//         .then(res => res.json())
//         .then(data => {
//             setUser({
//                 id: localStorage.setItem("id", data._id),
//                 email: localStorage.setItem("email", data.email),
//                 isAdmin: localStorage.setItem("isAdmin", data.isAdmin)
//             })
            
//         })
//     }


//     return (
//         (user.id !== null) ?
//             <Navigate to="/dashboard"/>
//         :
//         <>

//         <Container fluid className="w-100 m-0 p-0">
//             <Row className="w-100 m-0 p-0">
//                 <Col className="banner-bg vh-100 m-0 text-light d-flex flex-column align-items-center justify-content-center text-center p-5" xs={12} md={12} lg={8}>
//                     <img
//                         alt=""
//                         src={require('../images/backgroundhome.png')}
//                         height="800"
//                         className="rounded fluid"
//                      />
//                 </Col>

//                 <Col xs={12} md={12} lg={4} className="d-flex flex-column align-items-center justify-content-center m-0 colr-bg ">
//                     <div className='w-100 vh-50 p-5 shadow-sm shadow-lg rounded bg-white d-flex flex-column align-items-center justify-content-center'>
//                     <h1 className="my-3 text-center">LOGIN</h1>
            
//                     <Form className="w-100 pb-5" onSubmit={(e) => authenticate(e)}>
//                         <Form.Group controlId="userEmail" className="mb-3">
//                             <Form.Label>Email address</Form.Label>
//                             <Form.Control 
//                                 type="email" 
//                                 placeholder="Enter email"
//                                 value={email}
//                                 onChange={(e) => setEmail(e.target.value)}
//                                 required
//                             />
//                         </Form.Group>

//                         <Form.Group controlId="password" className="mb-3">
//                             <Form.Label>Password</Form.Label>
//                             <Form.Control 
//                                 type="password" 
//                                 placeholder="Password"
//                                 value={password}
//                                 onChange={(e) => setPassword(e.target.value)}
//                                 required
//                             />
//                         </Form.Group>

//             {
//             isActive
//             ?
//                 <Button variant="primary" className='px-2 py-2 w-25' type="submit" id="submitBtn">
//                     Login
//                 </Button>
//             :
//                 <Button variant="danger" className='px-2 py-2 w-25' type="submit" id="submitBtn" disabled>
//                     Login
//                 </Button>
//             }
//             <div className='d-flex flex-column align-items-center justify-content-center'>
//             <p className='mt-5'>No account yet? <Link to={"/register"}><strong>Click Here!</strong></Link></p>
//                     </div>
//                     </Form>
//                     </div>
//                 </Col>
//             </Row>
//         </Container>
//         </>
//     )
// }
