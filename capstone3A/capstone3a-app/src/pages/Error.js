import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";
import Banner from "../components/Banner";
import Branches from "../components/Branches";
import FeaturedProducts from "../components/FeaturedProducts";
import { Image } from "react-bootstrap";

export default function ErrorPage(){

	
	// const { user } = useContext(UserContext);
	const data = {
		title: "404: ERROR!",
		content: "Sorry, we can't find the page! Don't worry though, everything is STILL GLAZING AWESOME!.",
		destination: "/",
		label: "Home",
		image: <Image
                className="d-block w-50  carousel-height"
                src={require('../images/GlazedLogoGS.png')}
                fluid
                />
	}

	return(
		<>
		{
        (localStorage.getItem("isAdmin") === "true")
		?
		<Navigate to="/dashboard" />
		:
		<div>
			<Banner bannerProp={data}/>
			<FeaturedProducts />
			<Branches />
		</div>
		}
		</>
		
	)
}