import { useState, useEffect, useContext } from "react";
import {Container, Card, Button, Row, Col, Form} from "react-bootstrap";
import { useParams, useNavigate, Link, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import Banner from "../components/Banner";
import Footer from "../components/Footer";
import UserContext from "../UserContext";
import logo from "../images/GlazedLogo.png"
import 'animate.css';


export default function ProductView(){

	const data = {
		title: "YOU ARE ABOUT TO MAKE A TRANSACTION",
		content: "Buy products using your E-wallet. Less hassle, more efficient!",
		destination: "/products",
		label: "Other Products",
		image: {logo}
	}

	const { user } = useContext(UserContext);


	const { productId } = useParams();

	const navigate = useNavigate();


	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
    const [stocks, setStocks] = useState('');
    const [imgSource, setImg] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState(1);

	const [add, setAdd] = useState(true)
    const [dif, setDif] = useState(false)
    const [isActive, setIsActive] = useState(false)
    const [isAdmin, setIsAdmin] = useState(false)

    



    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: 'POST',
            headers: {Authorization: `Bearer ${user.token}`}
        }).then(res => res.json()).then(data => {
            setIsAdmin(data.isAdmin)
        })
    },[isAdmin, user.token])



	useEffect(()=>{
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProductName(data.name);
			// setModel(data.productModel);
	
      		setImg(data.imgSource);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId, productName, description, price])


	function increase(){
        if(stocks > quantity){
            setQuantity(quantity + 1)
            setAdd(true);
            setDif(true);
        } else {
            setAdd(false);
        }
    }

    function decrease(){
        if(quantity > 1){
            setQuantity(quantity - 1)
            setAdd(true);
        } else {
            setDif(false);
        }
    }

    function checkOutConfirmation(){
 
            Swal.fire({
                title: 'Order Successfully placed',
                text: "Thank you!",
                icon: "success",
                confirmButtonColor: '#379237',
                showClass: {
                popup: 'animate__animated animate__fadeIn'
                 },
                width: 400,
                padding: '1em',
                color: '#379237',
                hideClass: {
                popup: 'animate__animated animate__fadeOut'
                }
          }).then((result) => {
            if (isAdmin === false || user.token !== null) {
                checkOut()
            } else {
                <Navigate to='/' />
            }
          })
    }

    function checkOut(){
        fetch(`${process.env.REACT_APP_API_URL}/users/orderProduct`, {
            method: 'PUT',
            headers: {
                'Content-type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({

                productId: productId,
                quantity: quantity
                // totalAmount: price * quantity
            })
        }).then(res => {
            // updateProductStock()
        })
    }



	// const buy = (productId) => {
 //        fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
 //            method: "POST",
 //            headers: {
 //                "Content-Type": "application/json",
 //                Authorization: `Bearer ${localStorage.getItem('token')}`
 //            },
 //            body: JSON.stringify({
 //                totalAmount: price,
 //                products: [
 //                    {
 //                        productId: productId,
 //                        productName: productName,
 //                        quantity: quantity,
	// 						totalAmount: price * quantity
 //                    }
 //                ]
 //            })
 //        })
 //            .then(res => res.json())
 //            .then(data => {

 //                console.log(data);

   //           if (data) {
   //               Swal.fire({
			// 		title: "PRODUCT SOLD!",
   //          		icon: "success",
   //          		text: `Thank you for purchasing ${productName}.
   //             		A total of Php ${data.totalAmount} was deducted from your E-wallet.`,
   //        })
   //        // setStocks(data.stocks); // Update the stocks value from the response
   //        navigate("/products");
   // } else {
   //        Swal.fire({
   //          title: "Oops! Something went wrong",
   //          icon: "error",
   //          text: "An error occurred. Please try again.",
   //        })
   //      }
   //    });
   //  };

	return(
		
		<>
				{/* change "true" to true*/}
		{
				(localStorage.getItem("isAdmin") === true)

				?
					<Navigate to="/dashboard" />
				:
		<div>
		<Banner bannerProp={data} className="align-items-center justify-content-center"/>
		<div className="d-flex align-items-center justify-content-center flex-column">
			<Col className="my-2 " xs={12} md={12} lg={3}>
            <Card className="my-3 w-100  card-height shadow card-border shadow-md card-bg  pb-4">
            <Card.Img 
                src={imgSource}
            />
                <Card.Header >
                <Card.Title>
        		{productName}
                </Card.Title>
                </Card.Header>
                <Card.Body>
                <Card.Text>
                    {description}
                </Card.Text>
                <Card.Subtitle>
                    Price
                </Card.Subtitle>
                <Card.Text>
                    Php {price}
                </Card.Text>

				<Card.Subtitle>
                    Quantity
                </Card.Subtitle>
				<Form.Control className="w-25 my-2"
                    type="number"
                    required
					value={quantity}
                    onChange={(e) => setQuantity(Number(e.target.value))}
                />
{/*                <Button onClick={increase}>Increase</Button>
                <Button onClick={decrease}>Decrease</Button>*/}
                </Card.Body>
			<div className="text-center">
            <Card.Footer>
			
            {
	
				(user.id !== null)
				?
/*					<Button id="checkOutBtn" className='w-50 btn-outline-light' variant='success' onClick={() => buy(productId)}>CHECK OUT</Button>*/
					<Button id="checkOutBtn" className='w-50 btn-outline-light' variant='success' onClick={checkOutConfirmation}>CHECK OUT</Button>

				:
					<Button className='w-50 btn-outline-light my-3' variant="danger" as={Link} to={`/login`}>LOGIN TO BUY</Button>
			}
            </Card.Footer>
			</div>
			

        </Card>
            </Col>
			</div>
		<Footer />
		</div>
		}
		
		
		
		

		</>
	)
	
	}


	// const buy = (productId) => {

	// 	 console.log("Product ID:", productId); 

 //        fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
 //            method: "POST",
 //            headers: {
 //                "Content-Type": "application/json",
 //                Authorization: `Bearer ${localStorage.getItem('token')}`
 //            },
 //            body: JSON.stringify({
 //                // totalAmount: price,
 //                products: [
 //                    {
 //                        productId: productId,
 //                        // productName: name,
 //                        quantity: quantity,
	// 					// totalAmount: price * quantity
 //                    }
 //                ]
 //            })
 //        })
 //            .then(res => res.json())
 //            .then(data => {

 //                console.log(data);

 //                if (data) {
 //                    Swal.fire({
 //                        title: "PRODUCT SOLD!",
 //                        icon: "success",
 //                        text: `Thank you! Order has been placed!`,
 //                        confirmButtonColor: '#379237',
	// 	                showClass: {
	// 	                popup: 'animate__animated animate__fadeIn'
	// 	                 },
	// 	                width: 400,
	// 	                padding: '1em',
	// 	                color: '#379237',
	// 	                hideClass: {
	// 	                popup: 'animate__animated animate__fadeOut'
	// 	                }
 //                    })
 //                    navigate("/products");
 //                } else {
 //                    Swal.fire({
	// 	                title: "Something went wrong!",
	// 	                icon: "error",
	// 	                text: "Please try again.",
	// 	                confirmButtonColor: '#379237',
	// 	                showClass: {
	// 	                popup: 'animate__animated animate__fadeIn'
	// 	                 },
	// 	                width: 400,
	// 	                padding: '1em',
	// 	                color: '#379237',
	// 	                hideClass: {
	// 	                popup: 'animate__animated animate__fadeOut'
	// 	                }
 //                    })
 //                }
 //            });
 //    }
