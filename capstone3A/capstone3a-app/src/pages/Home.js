import { Navigate } from "react-router-dom";
import { useContext } from "react"
import UserContext from "../UserContext";

import Banner from "../components/Banner";
import Branches from "../components/Branches";
import HomeCarousel from "../components/HomeCarousel";
import FeaturedProducts from "../components/FeaturedProducts";
import Vloggers from "../components/Vloggers";
import Footer from "../components/Footer";

import logo from "../images/GlazedLogo.png"


export default function Home(){

	
	const { user } = useContext(UserContext);
	const data = {
		title: "Glazed Unlimited Wings",
		content: "The First Unlimited Wings with Cheese Fondue!",
		destination: "/products",
		label: "View our Full Menu!",
		image: {logo}
	}

	return(
		<>
		{
        (localStorage.getItem("isAdmin") === "true")
		?
		<Navigate to="/dashboard" />
		:
		<div>
			<HomeCarousel/>
			<Banner bannerProp={data}/>
			<Vloggers />
			<FeaturedProducts />
			<Branches />
			<Footer />
		</div>
		}
		</>
		
	)
}