import { useContext } from "react"
import UserContext from "../UserContext";
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
import {Container, Col, Row} from "react-bootstrap"
import { Navigate } from "react-router-dom";

import AppSideNav from "../components/AppSideNav";
import Banner from "../components/Banner";
import Branches from "../components/Branches";

import logo from "../images/GlazedLogo.png"

export default function Dashboard(){
     const { user } = useContext(UserContext);
    console.log(user);

    const data = {
		title: "WELCOME TO YOUR DASHBOARD!",
		content: "You can Add, edit, and archive a product on this page.",
		destination: "/dashboard",
		label: "DASHBOARD",
		image: {logo}
	}
	return(
        <>
        {
        (localStorage.getItem("isAdmin") === "true")
    ?
        <Container fluid className="w-100 m-0 p-0">
        <Row className="w-100 m-0 p-0">
        <Col className="bg-white vh-100 pt-3 m-0 shadow" xs={12} md={2} lg={2}>
        <AppSideNav/>

        </Col>
        <Col className="colr-bg vh-100 m-0 p-5" xs={12} md={10} lg={10}>
        <Banner bannerProp={data}/>
 
        </Col>
        
        </Row>
     </Container>
        :
        <Navigate to={"/"}/>
        }
   </>
  )
}