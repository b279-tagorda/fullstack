import { useEffect, useState, useContext } from "react";
import { Navigate } from "react-router-dom";
import {Row} from "react-bootstrap"

import Banner from "../components/Banner";
import ProductCard from "../components/ProductCard";
import Footer from "../components/Footer";
import UserContext from "../UserContext";

import logo from "../images/GlazedLogo.png"


export default function Products() {

	const data = {
		title: "Glazed Unlimited Wings",
		content: "The First Unlimited Wings with Cheese Fondue!",
		destination: "/products",
		label: "ORDER NOW!",
		image: {logo}
	}

	const { user } = useContext(UserContext);


	const [products, setProducts] = useState([]);

	useEffect(() =>{

		fetch(`${process.env.REACT_APP_API_URL}/products`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(products =>{
				return(
                        <ProductCard key={products._id} productProp={products}/>
				);
			}));
		})
	}, []);


	return(
		
		(localStorage.getItem("isAdmin") === "true")

		?
			<Navigate to="/dashboard" />
		:	
		<>
        <div className="p-5">
		<Banner bannerProp={data}/>
			<Row className="my-3 text-success h-100">
			{products}
			</Row>
        </div>
        <Footer />
		</>
	)
}









// // import courseData from "../data/courses"
// import ProductCard from '../components/ProductCard'
// import { useEffect, useState} from "react"


// export default function Products(){


// 	const [AllProducts, setAllProducts] = useState([]);

// 	// Retrieves the courses from database upon initial render of the "Courses" Component

// 	useEffect(() => {
// 		fetch(`http://localhost:4000/products/all`)
// 		.then(res => res.json())
// 		.then(data => {
// 			// console.log(data);


// 			setAllProducts(data.map(product => {
// 				return (
// 					<ProductCard key={product._id} productProp={product}/>
// 					)
// 			}))

// 		})
// 	}, [])


// 	return(
// 		<>
// 			<h1>Our Menu</h1>
// 			{/*Prop making ang prop passing*/}
// 			{AllProducts}
// 		</>
// 		)
// }